package pgxtx_test

import (
	"context"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/exa-bit/pkg/pgx-tx"
	"log"
)

func ExamplePgxInTransaction() {
	var db *pgxpool.Pool // assume db is valid

	txManager := pgxtx.NewPgxTransaction(db)
	err := txManager.InTransaction(context.TODO(), func(ctx context.Context) error {
		tx := pgxtx.FromContext(ctx)
		p := &post{
			Title: "My post",
		}
		stmt := `INSERT INTO ` + postTable + `(title) VALUES ($1) RETURNING id`
		var postId int
		err := tx.QueryRow(context.TODO(), stmt, p.Title).Scan(&postId)
		if err != nil {
			return err
		}

		cmt := &comment{
			PostID: postId,
			Review: "My review",
		}
		stmt = `INSERT INTO ` + commentTable + `(post_id, review) VALUES ($1, $2) RETURNING id`
		_, err = tx.Exec(context.TODO(), stmt, cmt.PostID, cmt.Review)
		if err != nil {
			return err // return error to rollback transaction
		}
		return nil // return nil (no error) will commit transaction
	}, pgx.TxOptions{})
	log.Fatal("cannot start transaction!", err)
}
