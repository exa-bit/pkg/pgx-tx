package pgxtx_test

import (
	"context"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/stretchr/testify/assert"
	"gitlab.com/exa-bit/pkg/pgx-tx"
	"log"
	"os"
	"testing"
)

func BenchmarkPgxInTransaction(b *testing.B) {
	db, err := pgxpool.Connect(context.TODO(), os.Getenv("PG_URI"))
	if err != nil {
		panic(err)
	}
	defer func() {
		dropPgxTestTables(db)
		db.Close()
	}()
	preparePgxTestEnvironment(db)

	txManager := pgxtx.NewPgxTransaction(db)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		err := txManager.InTransaction(context.TODO(), func(ctx context.Context) error {
			txc := pgxtx.FromContext(ctx)
			p := &post{
				Title: "My post",
			}
			insertPgxPost(txc, p)

			cmt := &comment{
				PostID: p.ID,
				Review: "My review",
			}
			insertPgxComment(txc, cmt)

			return nil
		}, pgx.TxOptions{})
		assert.NoError(b, err)
	}
}

// benchmark original pgx transaction
func BenchmarkPgxTransaction(b *testing.B) {
	db, err := pgxpool.Connect(context.TODO(), os.Getenv("PG_URI"))
	if err != nil {
		panic(err)
	}
	defer func() {
		dropPgxTestTables(db)
		db.Close()
	}()
	preparePgxTestEnvironment(db)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		tx, err := db.BeginTx(context.TODO(), pgx.TxOptions{})
		if err != nil {
			log.Fatal(err)
		}
		p := &post{
			Title: "My post",
		}
		insertPgxPost(tx, p)

		cmt := &comment{
			PostID: p.ID,
			Review: "My review",
		}
		insertPgxComment(tx, cmt)

		err = tx.Commit(context.TODO())
		assert.NoError(b, err)
	}
}

func BenchmarkPgxInTransaction_Parallel(b *testing.B) {
	db, err := pgxpool.Connect(context.TODO(), os.Getenv("PG_URI"))
	if err != nil {
		panic(err)
	}
	defer func() {
		dropPgxTestTables(db)
		db.Close()
	}()
	preparePgxTestEnvironment(db)
	txManager := pgxtx.NewPgxTransaction(db)

	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			err := txManager.InTransaction(context.TODO(), func(ctx context.Context) error {
				txc := pgxtx.FromContext(ctx)
				p := &post{
					Title: "My post",
				}
				insertPgxPost(txc, p)

				cmt := &comment{
					PostID: p.ID,
					Review: "My review",
				}
				insertPgxComment(txc, cmt)

				return nil
			}, pgx.TxOptions{})
			assert.NoError(b, err)
		}
	})
}

// benchmark original pgx transaction
func BenchmarkPgxTransaction_Parallel(b *testing.B) {
	db, err := pgxpool.Connect(context.TODO(), os.Getenv("PG_URI"))
	if err != nil {
		panic(err)
	}
	defer func() {
		dropPgxTestTables(db)
		db.Close()
	}()
	preparePgxTestEnvironment(db)

	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			tx, err := db.BeginTx(context.TODO(), pgx.TxOptions{})
			if err != nil {
				log.Fatal(err)
			}
			p := &post{
				Title: "My post",
			}
			insertPgxPost(tx, p)

			cmt := &comment{
				PostID: p.ID,
				Review: "My review",
			}
			insertPgxComment(tx, cmt)

			err = tx.Commit(context.TODO())
			assert.NoError(b, err)
		}
	})
}