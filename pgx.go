package pgxtx

import (
	"context"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
	"log"
)

type contextKey string

var (
	transactionKey      contextKey = "transactionKey"
	transactionDepthKey contextKey = "transactionDepthKey"
)

type TxStarter interface {
	BeginTx(ctx context.Context, txOptions pgx.TxOptions) (pgx.Tx, error)
}

type PgxTransaction struct {
	db *pgxpool.Pool
}

func NewPgxTransaction(db *pgxpool.Pool) *PgxTransaction {
	return &PgxTransaction{db: db}
}

/*
InTransaction creates and starts a new Session and use it to call the fn callback.
The Context contains `pgx.Tx` instance and should be used for any operations in the fn callback that supposed to be
executed under the session. To retrieve the `pgx.Tx` instance from ctx, use `FromContext` function.
If the ctx parameter already contains a Session, that Session will be reused instead effectively become 1 transaction.
To create new transaction instead of reused transaction, supply different ctx which doesn't contains any `pgx.Tx`.
Any error returned by the fn callback will abort (rollback) the transaction, otherwise commit it.
Original error will be returned if commit/rollback successful, otherwise will wrap the original error under commit/rollback
error.
The provided TxOptions is optional and may be zeroed if defaults should be used.
*/
func (s *PgxTransaction) InTransaction(ctx context.Context, fn func(context.Context) error, opts pgx.TxOptions) error {
	var commitErr error
	var txCtx context.Context

	tx := FromContext(ctx)
	depth := transactionDepthFromContext(ctx) + 1

	if tx == nil {
		var err error
		tx, err = s.db.BeginTx(ctx, opts)
		if err != nil {
			return err
		}

		txCtx = context.WithValue(ctx, transactionKey, tx)
	} else {
		txCtx = ctx
	}

	txCtx = context.WithValue(txCtx, transactionDepthKey, depth)

	panicked := true
	defer func() {
		// Make sure to rollback when panic, or Commit error
		if panicked || commitErr != nil {
			err := tx.Rollback(txCtx)

			if err != nil && err != pgx.ErrTxClosed {
				log.Print(errors.WithMessagef(err, "failed to rollback transaction: panicked: %v, commitErr: %v", panicked, commitErr))
			}
		}
	}()

	err := fn(txCtx)

	// if error then should rollback no matter how depth
	if err != nil {
		rollbackErr := tx.Rollback(txCtx)
		if rollbackErr != nil {
			err = errors.Wrap(err, rollbackErr.Error())
		}

		return err
	}

	// if no error then check if depth = 1, commit only when depth=1 (the most outer transactions)
	if depth == 1 {
		commitErr = tx.Commit(ctx)
		panicked = false

		return commitErr
	}

	panicked = false
	return nil
}

/*
PgxInTransaction creates and starts a new Session and use it to call the fn callback.
The Context contains `pgx.Tx` instance and should be used for any operations in the fn callback that supposed to be
executed under the session. To retrieve the `pgx.Tx` instance from ctx, use `FromContext` function.
If the ctx parameter already contains a Session, that Session will be reused instead effectively become 1 transaction.
To create new transaction instead of reused transaction, supply different ctx which doesn't contains any `pgx.Tx`.
Any error returned by the fn callback will abort (rollback) the transaction, otherwise commit it.
An error will be returned if there is error when starting transaction, performing commit or abort operation.
The provided TxOptions is optional and may be nil if defaults should be used.
*/
//func PgxInTransaction(ctx context.Context, db TxStarter, fn func(context.Context) error, opts pgx.TxOptions) error {
//	var commitErr error
//	var txCtx context.Context
//
//	tx := FromContext(ctx)
//	depth := transactionDepthFromContext(ctx) + 1
//
//	if tx == nil {
//		var err error
//		tx, err = db.BeginTx(ctx, opts)
//		if err != nil {
//			return err
//		}
//
//		txCtx = context.WithValue(ctx, transactionKey, tx)
//	} else {
//		txCtx = ctx
//	}
//
//	txCtx = context.WithValue(txCtx, transactionDepthKey, depth)
//
//	panicked := true
//	defer func() {
//		// Make sure to rollback when panic, or Commit error
//		if panicked || commitErr != nil {
//			err := tx.Rollback(txCtx)
//
//			if err != nil && err != pgx.ErrTxClosed {
//				log.Print(errors.WithMessagef(err, "failed to rollback transaction: panicked: %v, commitErr: %v", panicked, commitErr))
//			}
//		}
//	}()
//
//	err := fn(txCtx)
//
//	// if error then should rollback no matter how depth
//	if err != nil {
//		return tx.Rollback(txCtx)
//	}
//
//	// if no error then check if depth = 1, commit only when depth=1 (the most outer transactions)
//	if depth == 1 {
//		commitErr = tx.Commit(ctx)
//		panicked = false
//
//		return commitErr
//	}
//
//	panicked = false
//	return nil
//}

// FromContext return pgx.Tx instance (if any) from the specified context. Return nil if not found.
func FromContext(ctx context.Context) pgx.Tx {
	if tx, ok := ctx.Value(transactionKey).(pgx.Tx); ok {
		return tx
	}
	return nil
}

func transactionDepthFromContext(ctx context.Context) int {
	if depth, ok := ctx.Value(transactionDepthKey).(int); ok {
		return depth
	}
	return 0
}
