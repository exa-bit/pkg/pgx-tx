module gitlab.com/exa-bit/pkg/pgx-tx

go 1.14

require (
	github.com/jackc/pgtype v1.4.2
	github.com/jackc/pgx/v4 v4.8.1
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.5.1
	golang.org/x/crypto v0.0.0-20200709230013-948cd5f35899 // indirect
)
