package pgxtx_test

import (
	"context"
	"errors"
	"github.com/jackc/pgtype/pgxtype"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/stretchr/testify/assert"
	"gitlab.com/exa-bit/pkg/pgx-tx"
	"log"
	"os"
	"testing"
)

var (
	postTable    = "posts"
	commentTable = "comments"
)

type post struct {
	ID    int
	Title string
}

type comment struct {
	ID     int
	PostID int
	Review string
}

func try(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func dropPgxTestTables(db pgxtype.Querier) {
	_, _ = db.Exec(context.TODO(), `DROP TABLE `+commentTable)
	_, _ = db.Exec(context.TODO(), `DROP TABLE `+postTable)
}

func createPgxTestTables(db pgxtype.Querier) {
	_, _ = db.Exec(context.TODO(), `CREATE TABLE `+postTable+`(
		id serial not null primary key,
    	title varchar(250) not null
	)`)
	_, _ = db.Exec(context.TODO(), `CREATE TABLE `+commentTable+`(
		id serial not null primary key,
		post_id integer not null references posts(id),
		review varchar(250) not null
	)`)
}

func preparePgxTestEnvironment(db pgxtype.Querier) {
	dropPgxTestTables(db)
	createPgxTestTables(db)
}

func insertPgxPost(tx pgx.Tx, p *post) {
	stmt := `INSERT INTO ` + postTable + `(title) VALUES ($1) RETURNING id`
	var lastId int
	try(tx.QueryRow(context.TODO(), stmt, p.Title).Scan(&lastId))
	p.ID = lastId
}

func insertPgxComment(tx pgx.Tx, c *comment) {
	stmt := `INSERT INTO ` + commentTable + `(post_id, review) VALUES ($1, $2) RETURNING id`
	var lastId int
	try(tx.QueryRow(context.TODO(), stmt, c.PostID, c.Review).Scan(&lastId))
	c.ID = lastId
}

// test atomic commit multiple tables
func TestPgxInTransaction_AtomicCommit(t *testing.T) {
	db, err := pgxpool.Connect(context.TODO(), os.Getenv("PG_URI"))
	if err != nil {
		panic(err)
	}
	defer func() {
		dropPgxTestTables(db)
		db.Close()
	}()

	preparePgxTestEnvironment(db)

	txManager := pgxtx.NewPgxTransaction(db)
	var postId int
	err = txManager.InTransaction(context.TODO(), func(ctx context.Context) error {
		txc := pgxtx.FromContext(ctx)
		p := &post{
			Title: "My post",
		}
		insertPgxPost(txc, p)
		postId = p.ID

		cmt := &comment{
			PostID: postId,
			Review: "My review",
		}
		insertPgxComment(txc, cmt)

		return nil
	}, pgx.TxOptions{})
	if assert.NoError(t, err) {
		var posts = make([]*post, 0)
		rows, err := db.Query(context.TODO(), "SELECT id, title FROM "+postTable)
		try(err)
		for rows.Next() {
			var p = new(post)
			try(rows.Scan(&p.ID, &p.Title))
			posts = append(posts, p)
		}

		assert.Len(t, posts, 1)

		var comments = make([]*comment, 0)
		rows, err = db.Query(context.TODO(), "SELECT id, post_id, review FROM "+commentTable)
		try(err)
		for rows.Next() {
			var cmt = new(comment)
			try(rows.Scan(&cmt.ID, &cmt.PostID, &cmt.Review))
			comments = append(comments, cmt)
		}
		assert.Len(t, comments, 1)
		assert.Equal(t, comments[0].PostID, postId)
	}
}

// test atomic rollback multiple tables
func TestPgxInTransaction_AtomicRollback(t *testing.T) {
	db, err := pgxpool.Connect(context.TODO(), os.Getenv("PG_URI"))
	if err != nil {
		panic(err)
	}
	defer func() {
		dropPgxTestTables(db)
		db.Close()
	}()

	preparePgxTestEnvironment(db)

	txManager := pgxtx.NewPgxTransaction(db)
	var postId int
	err = txManager.InTransaction(context.TODO(), func(ctx context.Context) error {
		txc := pgxtx.FromContext(ctx)
		p := &post{
			Title: "My post",
		}
		insertPgxPost(txc, p)
		postId = p.ID

		cmt := &comment{
			PostID: postId,
			Review: "My review",
		}
		insertPgxComment(txc, cmt)

		return errors.New("should rollback")
	}, pgx.TxOptions{})
	if assert.EqualError(t, err, "should rollback") {
		var posts = make([]*post, 0)
		rows, err := db.Query(context.TODO(), "SELECT id, title FROM "+postTable)
		try(err)
		for rows.Next() {
			var p = new(post)
			try(rows.Scan(&p.ID, &p.Title))
			posts = append(posts, p)
		}
		assert.Len(t, posts, 0)

		var comments = make([]*comment, 0)
		rows, err = db.Query(context.TODO(), "SELECT id, post_id, review FROM "+commentTable)
		try(err)
		for rows.Next() {
			var cmt = new(comment)
			try(rows.Scan(&cmt.ID, &cmt.PostID, &cmt.Review))
			comments = append(comments, cmt)
		}
		assert.Len(t, comments, 0)
	}
}

// test two nested transactions, the inner session should committed while the outer session should rollback
func TestPgxInTransaction_NestedSession(t *testing.T) {
	db, err := pgxpool.Connect(context.TODO(), os.Getenv("PG_URI"))
	if err != nil {
		panic(err)
	}
	defer func() {
		dropPgxTestTables(db)
		db.Close()
	}()

	preparePgxTestEnvironment(db)

	txManager := pgxtx.NewPgxTransaction(db)
	err = txManager.InTransaction(context.TODO(), func(ctx context.Context) error {
		txc := pgxtx.FromContext(ctx)
		p := &post{
			Title: "My post",
		}
		insertPgxPost(txc, p)
		postId := p.ID

		cmt := &comment{
			PostID: postId,
			Review: "My review",
		}
		insertPgxComment(txc, cmt)

		err = txManager.InTransaction(context.TODO(), func(ctx2 context.Context) error {
			txc2 := pgxtx.FromContext(ctx2)
			p := &post{
				Title: "My post 2",
			}
			insertPgxPost(txc2, p)
			postId2 := p.ID

			cmt := &comment{
				PostID: postId2,
				Review: "My review 2",
			}
			insertPgxComment(txc2, cmt)

			return nil
		}, pgx.TxOptions{})
		assert.NoError(t, err)

		return errors.New("should rollback")
	}, pgx.TxOptions{})
	if assert.EqualError(t, err, "should rollback") {
		var posts = make([]*post, 0)
		rows, err := db.Query(context.TODO(), "SELECT id, title FROM "+postTable)
		try(err)
		for rows.Next() {
			var p = new(post)
			try(rows.Scan(&p.ID, &p.Title))
			posts = append(posts, p)
		}

		assert.Len(t, posts, 1)
		assert.Equal(t, "My post 2", posts[0].Title)

		var comments = make([]*comment, 0)
		rows, err = db.Query(context.TODO(), "SELECT id, post_id, review FROM "+commentTable)
		try(err)
		for rows.Next() {
			var cmt = new(comment)
			try(rows.Scan(&cmt.ID, &cmt.PostID, &cmt.Review))
			comments = append(comments, cmt)
		}
		assert.Len(t, comments, 1)
		assert.Equal(t, "My review 2", comments[0].Review)
	}
}

// test two nested transactions with reused ctx which should execute operations in one transaction. This test should
// commit all operations.
func TestPgxInTransaction_NestedSession_Reused_AtomicCommit(t *testing.T) {
	db, err := pgxpool.Connect(context.TODO(), os.Getenv("PG_URI"))
	if err != nil {
		panic(err)
	}
	defer func() {
		dropPgxTestTables(db)
		db.Close()
	}()

	preparePgxTestEnvironment(db)

	txManager := pgxtx.NewPgxTransaction(db)
	err = txManager.InTransaction(context.TODO(), func(ctx context.Context) error {
		txc := pgxtx.FromContext(ctx)
		p := &post{
			Title: "My post",
		}
		insertPgxPost(txc, p)
		postId := p.ID

		cmt := &comment{
			PostID: postId,
			Review: "My review",
		}
		insertPgxComment(txc, cmt)

		err = txManager.InTransaction(ctx, func(ctx2 context.Context) error {
			txc2 := pgxtx.FromContext(ctx2)
			p := &post{
				Title: "My post 2",
			}
			insertPgxPost(txc2, p)
			postId2 := p.ID

			cmt := &comment{
				PostID: postId2,
				Review: "My review 2",
			}
			insertPgxComment(txc2, cmt)

			return nil
		}, pgx.TxOptions{})
		assert.NoError(t, err)

		return nil
	}, pgx.TxOptions{})
	if assert.NoError(t, err) {
		var posts = make([]*post, 0)
		rows, err := db.Query(context.TODO(), "SELECT id, title FROM "+postTable)
		try(err)
		for rows.Next() {
			var p = new(post)
			try(rows.Scan(&p.ID, &p.Title))
			posts = append(posts, p)
		}

		assert.Len(t, posts, 2)
		assert.Equal(t, "My post", posts[0].Title)

		var comments = make([]*comment, 0)
		rows, err = db.Query(context.TODO(), "SELECT id, post_id, review FROM "+commentTable)
		try(err)
		for rows.Next() {
			var cmt = new(comment)
			try(rows.Scan(&cmt.ID, &cmt.PostID, &cmt.Review))
			comments = append(comments, cmt)
		}
		assert.Len(t, comments, 2)
		assert.Equal(t, "My review", comments[0].Review)
	}
}

// test two nested transactions with reused ctx which should execute operations in one transaction. This test should
// rollback all operations.
func TestPgxInTransaction_NestedSession_Reused_AtomicRollback(t *testing.T) {
	db, err := pgxpool.Connect(context.TODO(), os.Getenv("PG_URI"))
	if err != nil {
		panic(err)
	}
	defer func() {
		dropPgxTestTables(db)
		db.Close()
	}()

	preparePgxTestEnvironment(db)

	txManager := pgxtx.NewPgxTransaction(db)
	err = txManager.InTransaction(context.TODO(), func(ctx context.Context) error {
		txc := pgxtx.FromContext(ctx)
		p := &post{
			Title: "My post",
		}
		insertPgxPost(txc, p)
		postId := p.ID

		cmt := &comment{
			PostID: postId,
			Review: "My review",
		}
		insertPgxComment(txc, cmt)

		err = txManager.InTransaction(ctx, func(ctx2 context.Context) error {
			txc2 := pgxtx.FromContext(ctx2)
			p := &post{
				Title: "My post 2",
			}
			insertPgxPost(txc2, p)
			postId2 := p.ID

			cmt := &comment{
				PostID: postId2,
				Review: "My review 2",
			}
			insertPgxComment(txc2, cmt)

			return nil
		}, pgx.TxOptions{})
		assert.NoError(t, err)

		return errors.New("should rollback")
	}, pgx.TxOptions{})
	if assert.EqualError(t, err, "should rollback") {
		var posts = make([]*post, 0)
		rows, err := db.Query(context.TODO(), "SELECT id, title FROM "+postTable)
		try(err)
		for rows.Next() {
			var p = new(post)
			try(rows.Scan(&p.ID, &p.Title))
			posts = append(posts, p)
		}

		assert.Len(t, posts, 0)

		var comments = make([]*comment, 0)
		rows, err = db.Query(context.TODO(), "SELECT id, post_id, review FROM "+commentTable)
		try(err)
		for rows.Next() {
			var cmt = new(comment)
			try(rows.Scan(&cmt.ID, &cmt.PostID, &cmt.Review))
			comments = append(comments, cmt)
		}
		assert.Len(t, comments, 0)
	}
}