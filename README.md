# exa-bit/pkg/pgx-tx

Contains simplified closure based transaction support for `pgx`.
The aim is to provide similar function signature to work with pgx transaction as with another database (ie. mongodb and sql).

## Tested On
- Postgres 12 - `github.com/jackc/pgx/v4 v4.7.2`
